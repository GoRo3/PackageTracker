# PackageTracker

This is project to learn basics of Java language.

# Project task

Create Java application based on Gradle or Maven for sending and tracking a parcels. Don't think about GUI right now, instead of that, create backend services.

* Parcel should have unique id, dimension (AxBxC), short description of the content, recipient info and address, date of dispatch, last modification date, type of shipment (Normal, Priority), Status (New, ready to send, sent, ready to receive, received), sender info, postman info - who take the parcel. 
* Create service for manage a parcel. Add methods: send a new parcel, change status, pay for the parcel (after sent parcel and only when parcel is already paid you can change status)
* Tracking service for receive information about the parcel (also about price). The customer can put parcel Id and see information about the parcel.
* Pricing service for price calculation. Create an algorithm:  Set base price (this value should be stored in application.properties file). Take the longest side of the package and multiply base price (if the longest side is between value a and value b). Priority shipping should be multiply 2 and if the parcel will be sent to another country again multiply 2. You can check how it works on the right side.
* Instead of the database, you can store all information on lists. 
* Remember about logging all important informations.